import request from '@/utils/request'

// 查询自媒体图文素材信息列表
export function listMaterial(query) {
  return request({
    url: '/wemedia/material/list',
    method: 'get',
    params: query
  })
}

// 查询自媒体图文素材信息详细
export function getMaterial(id) {
  return request({
    url: '/wemedia/material/' + id,
    method: 'get'
  })
}

// 新增自媒体图文素材信息
export function addMaterial(data) {
  return request({
    url: '/wemedia/material',
    method: 'post',
    data: data
  })
}

// 修改自媒体图文素材信息
export function updateMaterial(data) {
  return request({
    url: '/wemedia/material',
    method: 'put',
    data: data
  })
}

// 删除自媒体图文素材信息
export function delMaterial(id) {
  return request({
    url: '/wemedia/material/' + id,
    method: 'delete'
  })
}
