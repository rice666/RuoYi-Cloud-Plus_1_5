import request from '@/utils/request'

// 查询频道信息列表
export function listChannel(query) {
  return request({
    url: '/wemedia/channel/list',
    method: 'get',
    params: query
  })
}

// 查询频道信息列表
export function channelList4News() {
  return request({
    url: '/wemedia/channel/channellist',
    method: 'get'
  })
}

// 查询频道信息详细
export function getChannel(id) {
  return request({
    url: '/wemedia/channel/' + id,
    method: 'get'
  })
}

// 新增频道信息
export function addChannel(data) {
  return request({
    url: '/wemedia/channel',
    method: 'post',
    data: data
  })
}

// 修改频道信息
export function updateChannel(data) {
  return request({
    url: '/wemedia/channel',
    method: 'put',
    data: data
  })
}

// 删除频道信息
export function delChannel(id) {
  return request({
    url: '/wemedia/channel/' + id,
    method: 'delete'
  })
}
