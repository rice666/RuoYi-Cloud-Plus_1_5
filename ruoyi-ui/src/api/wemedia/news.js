import request from '@/utils/request'

// 查询文章列表列表
export function listNews(query) {
  return request({
    url: '/wemedia/news/list',
    method: 'get',
    params: query
  })
}

// 查询文章列表详细
export function getNews(id) {
  return request({
    url: '/wemedia/news/' + id,
    method: 'get'
  })
}

// 新增文章列表
export function addNews(data) {
  return request({
    url: '/wemedia/news',
    method: 'post',
    data: data
  })
}

// 修改文章列表
export function updateNews(data) {
  return request({
    url: '/wemedia/news',
    method: 'put',
    data: data
  })
}

// 删除文章列表
export function delNews(id) {
  return request({
    url: '/wemedia/news/' + id,
    method: 'delete'
  })
}

// 测试阿里云审核文本
export function ali_text() {
  return request({
    url: '/wemedia/news/ali_text',
    method: 'get'
  })
}

// 修改文章列表
export function down_or_up(data) {
  return request({
    url: '/wemedia/news/down_or_up',
    method: 'post',
    data: data
  })
}
