/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.0.60
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : 192.168.0.60:3306
 Source Schema         : ry_news_article

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 09/04/2023 09:57:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ap_article_content
-- ----------------------------
DROP TABLE IF EXISTS `ap_article_content`;
CREATE TABLE `ap_article_content`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `article_id` bigint UNSIGNED NULL DEFAULT NULL COMMENT '文章ID',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '文章内容',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_article_id`(`article_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1638517880971403266 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = 'APP已发布文章内容表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ap_article_content
-- ----------------------------
INSERT INTO `ap_article_content` VALUES (1634417822357319682, 1634417821904334850, '[{\"type\":\"image\",\"value\":\"http://192.168.0.60:9000/ruoyi/2023/03/11/be7c6141ee224482a5b9ca84b324cc0d.jpg\"},{\"type\":\"text\",\"value\":\"这是一面悬挂在临时早餐摊位上的小店招，摊位由一对在奉贤打拼的河南90后小夫妻经营。日前，奉贤一位摄影爱好者在采风中，为小夫妻俩“对生活的热爱和自信”而感动，拍摄了一早摊位上热气腾腾的忙碌画面，上传朋友圈后，获得了不少好友的纷纷点赞，有市民留言：有理想的摊主。\"},{\"type\":\"text\",\"value\":\"近日一早，小编也实地探访了这家奔赴在“百年老店”旅途中的早餐摊位。\"}]');
INSERT INTO `ap_article_content` VALUES (1634779600597667841, 1634779600530558977, '[{\"type\":\"text\",\"value\":\"全国两会上，0-3岁婴幼儿托育再次关注的焦点。多个代表、委员提出加快建立普惠托育服务体系的建议。\"},{\"type\":\"image\",\"value\":\"http://192.168.0.60:9000/ruoyi/2023/03/12/e45f9ed2db2d41ddb713f9df7891871b.jpg\"},{\"type\":\"text\",\"value\":\"全国政协委员、全国台联第十一届理事会常务理事陈小艳提出幼儿园入园年龄低龄化的建议。她表示，幼儿园规定三岁儿童才能入园，某种程度上制约了许多妈妈无法在产后尽快参与到社会生产生活当中，限制了个人能力的发挥。\"}]');
INSERT INTO `ap_article_content` VALUES (1634780990426435586, 1634780990384492545, '[{\"type\":\"text\",\"value\":\"“最美樱花季”将至！玉渊潭公园呼吁提前预约\"}]');
INSERT INTO `ap_article_content` VALUES (1634781313056493569, 1634781313027133441, '[{\"type\":\"text\",\"value\":\"近期气温一再攀升，春天“提前”到来了吗？\"}]');
INSERT INTO `ap_article_content` VALUES (1634781746256793602, 1634781746206461953, '[{\"type\":\"text\",\"value\":\"北京玉渊潭山桃花绽放 满园春色迎客来\"}]');
INSERT INTO `ap_article_content` VALUES (1635915347992059905, 1635915347908173825, '[{\"type\":\"text\",\"value\":\"北京新闻\"}]');
INSERT INTO `ap_article_content` VALUES (1635919604933779458, 1635919604812144641, '[{\"type\":\"text\",\"value\":\"北京咨询\"}]');
INSERT INTO `ap_article_content` VALUES (1635922002586673154, 1635922002465038338, '[{\"type\":\"text\",\"value\":\"河南新闻\"}]');
INSERT INTO `ap_article_content` VALUES (1636280699955564545, 1636280699863289858, '[{\"type\":\"text\",\"value\":\"重磅消息\"}]');
INSERT INTO `ap_article_content` VALUES (1636281472550555650, 1636281472491835394, '[{\"type\":\"text\",\"value\":\"河南春天美景一览\"}]');
INSERT INTO `ap_article_content` VALUES (1636296321259130881, 1636296321212993537, '[{\"type\":\"text\",\"value\":\"北京故宫春天美景美不胜收\"},{\"type\":\"image\",\"value\":\"http://192.168.0.60:9000/ruoyi/2023/03/12/4a15fe76e2294a34a64596497748adf6.jpg\"}]');
INSERT INTO `ap_article_content` VALUES (1638433074740797442, 1638433074656911361, '[{\"type\":\"text\",\"value\":\"北京故宫春天美景美不胜收\"},{\"type\":\"image\",\"value\":\"http://192.168.0.60:9000/ruoyi/2023/03/12/4a15fe76e2294a34a64596497748adf6.jpg\"}]');
INSERT INTO `ap_article_content` VALUES (1638517880971403265, 1638517880929460226, '[{\"type\":\"text\",\"value\":\"如题，小山风景美如画，哈哈哈哈\"}]');

SET FOREIGN_KEY_CHECKS = 1;
