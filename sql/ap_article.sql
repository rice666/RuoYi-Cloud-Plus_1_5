/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.0.60
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : 192.168.0.60:3306
 Source Schema         : ry_news_article

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 09/04/2023 09:57:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ap_article
-- ----------------------------
DROP TABLE IF EXISTS `ap_article`;
CREATE TABLE `ap_article`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `author_id` int UNSIGNED NULL DEFAULT NULL COMMENT '文章作者的ID',
  `author_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '作者昵称',
  `channel_id` int UNSIGNED NULL DEFAULT NULL COMMENT '文章所属频道ID',
  `channel_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '频道名称',
  `layout` tinyint UNSIGNED NULL DEFAULT NULL COMMENT '文章布局\r\n            0 无图文章\r\n            1 单图文章\r\n            2 多图文章',
  `flag` tinyint UNSIGNED NULL DEFAULT NULL COMMENT '文章标记\r\n            0 普通文章\r\n            1 热点文章\r\n            2 置顶文章\r\n            3 精品文章\r\n            4 大V 文章',
  `images` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文章图片\r\n            多张逗号分隔',
  `labels` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文章标签最多3个 逗号分隔',
  `likes` int UNSIGNED NULL DEFAULT NULL COMMENT '点赞数量',
  `collection` int UNSIGNED NULL DEFAULT NULL COMMENT '收藏数量',
  `comment` int UNSIGNED NULL DEFAULT NULL COMMENT '评论数量',
  `views` int UNSIGNED NULL DEFAULT NULL COMMENT '阅读数量',
  `province_id` int UNSIGNED NULL DEFAULT NULL COMMENT '省市',
  `city_id` int UNSIGNED NULL DEFAULT NULL COMMENT '市区',
  `county_id` int UNSIGNED NULL DEFAULT NULL COMMENT '区县',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `publish_time` datetime NULL DEFAULT NULL COMMENT '发布时间',
  `sync_status` tinyint(1) NULL DEFAULT 0 COMMENT '同步状态',
  `origin` tinyint UNSIGNED NULL DEFAULT 0 COMMENT '来源',
  `static_url` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1638517880929460227 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '文章信息表，存储已发布的文章' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ap_article
-- ----------------------------
INSERT INTO `ap_article` VALUES (1634417821904334850, '这对河南90后小夫妻要在奉贤经营“百年老店”', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/03/11/c32fb596e5ae4d2aafb6def973124785.jpg', '日常', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-11 12:55:49', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1634779600530558977, '两岁就上幼儿园，可行吗？', 1, 'admin', NULL, '其他', 3, NULL, 'http://192.168.0.60:9000/ruoyi/2023/03/12/e45f9ed2db2d41ddb713f9df7891871b.jpg,http://192.168.0.60:9000/ruoyi/2023/03/12/920fa64cf7304401992d1c2cb7f38254.jpg,http://192.168.0.60:9000/ruoyi/2023/03/12/f554a7782c89498995e679f65a967036.jpg', '生活', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-12 12:53:24', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1634780990384492545, '“最美樱花季”将至！玉渊潭公园呼吁提前预约', 1, 'admin', NULL, '其他', 0, NULL, NULL, '娱乐', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-12 12:58:55', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1634781313027133441, '近期气温一再攀升，春天“提前”到来了吗？', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/03/12/629af62948834b5aadaf0101f769aaec.jpg', '其他', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-12 13:00:12', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1634781746206461953, '北京玉渊潭山桃花绽放 满园春色迎客来', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/03/12/4a15fe76e2294a34a64596497748adf6.jpg', '其他', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-12 13:01:56', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1635915347908173825, '北京新闻', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/02/27/07a8b75a864440f582f29b366b23f460.jpg', '其他', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-15 16:06:27', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1635919604812144641, '北京咨询', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/02/27/8550dc47e39c4079a0086e62d4eb6aa6.jpg', '人文', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-15 16:23:22', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1635922002465038338, '河南新闻', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/02/27/07a8b75a864440f582f29b366b23f460.jpg', '其他', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-15 16:32:54', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1636280699863289858, '河南重磅消息', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/02/27/c4aa574f60bb41eea6ecbcc06b4625a9.jpg', '其他', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-16 16:18:14', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1636281472491835394, '河南春天美景一览', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/02/27/c4aa574f60bb41eea6ecbcc06b4625a9.jpg', '其他', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-16 16:21:18', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1636296321212993537, '北京故宫春天美景美不胜收', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/02/27/2a056ceefbd24a608e0d40d5ec5cf4d2.jpg', '新闻', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-16 17:20:19', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1638433074656911361, '北京故宫春天美景美不胜收', 1, 'admin', NULL, '其他', 1, NULL, 'http://192.168.0.60:9000/ruoyi/2023/02/27/2a056ceefbd24a608e0d40d5ec5cf4d2.jpg', '新闻', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-22 14:51:00', NULL, 0, 0, NULL);
INSERT INTO `ap_article` VALUES (1638517880929460226, '小山风景美如画', 1, 'admin', NULL, '其他', 0, NULL, NULL, '其他', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2023-03-22 20:28:00', NULL, 0, 0, NULL);

SET FOREIGN_KEY_CHECKS = 1;
