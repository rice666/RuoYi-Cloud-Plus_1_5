package com.ruoyi.article.api;

import com.ruoyi.article.api.model.ArticleDto;
import com.ruoyi.common.constants.AppHttpCodeEnum;
import com.ruoyi.common.core.domain.R;


public class IArticleClientMock implements IArticleClient{
    @Override
    public R<Long> saveArticle(ArticleDto dto) {
        System.err.println("熔断--------------------------------------------------");
        return R.fail(AppHttpCodeEnum.SERVER_ERROR + "获取数据失败");
    }
}
