package com.ruoyi.article.api.model;

import com.ruoyi.article.api.domain.ApArticle;
import lombok.Data;

@Data
public class ArticleDto  extends ApArticle {
    /**
     * 文章内容
     */
    private String content;
}
