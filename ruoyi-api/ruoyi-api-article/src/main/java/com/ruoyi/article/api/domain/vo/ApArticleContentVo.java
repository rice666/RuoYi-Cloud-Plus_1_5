package com.ruoyi.article.api.domain.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ApArticleContentVo implements Serializable {

    private Long id;

    /**
     * 文章id
     */
    private Long articleId;

    /**
     * 文章内容
     */
    private String content;
}
