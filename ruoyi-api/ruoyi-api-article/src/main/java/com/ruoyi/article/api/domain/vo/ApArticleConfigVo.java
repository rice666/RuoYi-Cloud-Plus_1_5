package com.ruoyi.article.api.domain.vo;


import lombok.Data;


/**
 * APP已发布文章配置视图对象
 *
 * @author xiaoqiang
 * @date 2022-11-11
 */
@Data

public class ApArticleConfigVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */

    private Long id;

    /**
     * 文章ID
     */

    private Long articleId;

    /**
     * 是否可评论
     */

    private String isComment;

    /**
     * 是否转发
     */

    private String isForward;

    /**
     * 是否下架
     */

    private String isDown;

    /**
     * 是否已删除
     */

    private String isDelete;


}
