package com.ruoyi.article.api;

import com.ruoyi.article.api.model.ArticleDto;
import com.ruoyi.common.core.domain.R;
import org.springframework.web.bind.annotation.RequestBody;

public interface IArticleClient {

    public R<Long> saveArticle(@RequestBody ArticleDto dto) ;
}
