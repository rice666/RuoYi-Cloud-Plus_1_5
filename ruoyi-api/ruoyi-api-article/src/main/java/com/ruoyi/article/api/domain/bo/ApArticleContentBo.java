package com.ruoyi.article.api.domain.bo;

import lombok.Data;

/**
 * APP已发布文章内容业务对象
 *
 * @author xiaoqiang
 * @date 2022-10-30
 */

@Data
public class ApArticleContentBo {

    /**
     * 主键
     */
    //@NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 文章ID
     */
    //@NotNull(message = "文章ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long articleId;

    /**
     * 文章内容
     */
    //@NotBlank(message = "文章内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String content;


}
