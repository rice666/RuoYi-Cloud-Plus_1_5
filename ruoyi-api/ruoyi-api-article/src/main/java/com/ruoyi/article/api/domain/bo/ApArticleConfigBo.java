package com.ruoyi.article.api.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * APP已发布文章配置业务对象
 *
 * @author xiaoqiang
 * @date 2022-11-11
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ApArticleConfigBo extends BaseEntity {

    /**
     * 主键
     */
    @NotBlank(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 文章ID
     */
    @NotBlank(message = "文章ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long articleId;

    /**
     * 是否可评论
     */
    @NotBlank(message = "是否可评论不能为空", groups = { AddGroup.class, EditGroup.class })
    private String isComment;

    /**
     * 是否转发
     */
    @NotBlank(message = "是否转发不能为空", groups = { AddGroup.class, EditGroup.class })
    private String isForward;

    /**
     * 是否下架
     */
    @NotBlank(message = "是否下架不能为空", groups = { AddGroup.class, EditGroup.class })
    private String isDown;

    /**
     * 是否已删除
     */
    @NotBlank(message = "是否已删除不能为空", groups = { AddGroup.class, EditGroup.class })
    private String isDelete;


}
