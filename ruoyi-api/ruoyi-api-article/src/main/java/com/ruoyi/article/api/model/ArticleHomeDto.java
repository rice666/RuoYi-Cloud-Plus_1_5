package com.ruoyi.article.api.model;

import lombok.Data;

import java.util.Date;

/**
 * 首页加载DTO
 */
@Data
public class ArticleHomeDto {

    //最大时间  （加载最新）
    private Date maxBehotTime;

    //最小时间  （加载更多）
    private Date minBehotTime;

    //分页大小
    private Integer size;

    //频道ID  （__all__ 代表是推荐分类）
    private String tag;

}
