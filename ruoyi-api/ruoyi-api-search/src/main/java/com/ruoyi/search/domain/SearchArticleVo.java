package com.ruoyi.search.domain;

import cn.easyes.annotation.HighLight;
import cn.easyes.annotation.IndexField;
import cn.easyes.annotation.IndexId;
import cn.easyes.annotation.IndexName;

import cn.easyes.annotation.rely.Analyzer;
import cn.easyes.annotation.rely.FieldType;
import cn.easyes.annotation.rely.IdType;
import cn.easyes.common.utils.StringUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@IndexName
public class SearchArticleVo implements Serializable {

    // 文章id
    @IndexId(type = IdType.CUSTOMIZE)
    private Long id;
    // 文章标题
    @HighLight(mappingField = "h_title",preTag = "<text style='color:red'>",postTag = "</text>")
    @IndexField(fieldType = FieldType.TEXT, analyzer = Analyzer.IK_SMART, searchAnalyzer = Analyzer.IK_MAX_WORD)
    private String title;
    // 文章发布时间
    @IndexField(fieldType = FieldType.DATE)
    private Date publishTime;
    // 文章布局
    @IndexField(fieldType = FieldType.INTEGER)
    private Integer layout;
    // 封面
    @IndexField(fieldType = FieldType.KEYWORD)
    private String images;
    // 作者id
    @IndexField(fieldType = FieldType.LONG)
    private Long authorId;
    // 作者名词
    @IndexField(fieldType = FieldType.TEXT)
    private String authorName;
    //静态url
    @IndexField(fieldType = FieldType.KEYWORD)
    private String staticUrl;
    //文章内容
    @IndexField(fieldType = FieldType.TEXT, analyzer = Analyzer.IK_SMART, searchAnalyzer = Analyzer.IK_MAX_WORD)
    private String content;

    @IndexField(exist = false)
    private String h_title;

    public String getH_title() {
        return StringUtils.isBlank(this.h_title) ? this.title : h_title;
    }
}
