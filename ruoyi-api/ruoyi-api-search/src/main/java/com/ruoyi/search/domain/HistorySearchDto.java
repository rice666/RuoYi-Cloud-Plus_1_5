package com.ruoyi.search.domain;

import lombok.Data;

@Data
public class HistorySearchDto {
    /**
    * 接收搜索历史记录id
    */
    String id;
}
