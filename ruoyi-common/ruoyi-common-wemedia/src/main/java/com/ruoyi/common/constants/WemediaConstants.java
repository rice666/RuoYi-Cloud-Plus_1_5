package com.ruoyi.common.constants;

/**
 * 自媒体文章发布常量类
 */
public class WemediaConstants {

    /**
     * 封面类型  1.无图， 2.单图  3.多图  4.自动
     */
    public static final Short WM_NEWS_NONE_IMAGE = 0;
    public static final Short WM_NEWS_SINGLE_IMAGE = 1;
    public static final Short WM_NEWS_MANY_IMAGE = 3;
    public static final Short WM_NEWS_AUTO_IMAGE = -1;


    /**
     * 素材的关系类型  0.内容素材  1.封面素材
     */
    public static final Short WM_CONTENT_REFERENCE = 0;
    public static final Short WM_COVER_REFERENCE = 1;


}
