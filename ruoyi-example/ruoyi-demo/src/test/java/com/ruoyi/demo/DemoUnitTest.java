package com.ruoyi.demo;

import com.ruoyi.common.redis.utils.RedisUtils;
import com.ruoyi.demo.domain.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.redisson.Redisson;
import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 单元测试案例
 *
 * @author Lion Li
 */
@SpringBootTest // 此注解只能在 springboot 主包下使用 需包含 main 方法与 yml 配置文件
@DisplayName("单元测试案例")
@Slf4j
public class DemoUnitTest {

    @Value("${spring.application.name}")
    private String appName;

    @DisplayName("测试 Redission RBucket")
    @Test
    public void testRedission() {
        //RedisUtils.setCacheObject("这是key","这是value", Duration.ofSeconds(60));
        RedissonClient redissonClient = RedisUtils.getClient();
        RBucket<Object> bucket = redissonClient.getBucket("key_name");
        //bucket.set("zhangsan",30, TimeUnit.SECONDS);
        Person person = new Person();
        person.setName("zhangsan");
        person.setAge(50);
        bucket.set(person,60, TimeUnit.SECONDS);
        //Object value = bucket.get();
        //System.out.println(value);
        //通过key取value值
        Object obj = redissonClient.getBucket("key_name").get();
        Person person1 = (Person)obj;
        System.out.println(person1.getName());

        //关闭客户端
        redissonClient.shutdown();

    }

    @DisplayName("测试 RedissionStream")
    @Test
    public void testRedissionStream() throws Exception{
        RedissonClient redissonClient = RedisUtils.getClient();
        //====================操作流来存储对象====================
        RBinaryStream stream = redissonClient.getBinaryStream("stream");
        //不会累加
        stream.set("name is ".getBytes());
        stream.set("name is ".getBytes());
        OutputStream outputStream = stream.getOutputStream();

        //会累加
        outputStream.write("victory".getBytes());
        outputStream.write("victory".getBytes());
        InputStream inputStream = stream.getInputStream();

        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] bytes = new byte[1024];
        int length;
        while ((length = inputStream.read(bytes)) != -1) {
            result.write(bytes, 0, length);
        }
        System.out.println(result.toString());


    }
    @DisplayName("测试 Redission RBatch")
    @Test
    public void testRedissionBatch() throws ExecutionException, InterruptedException {

        /*RedissonClient redissonClient = RedisUtils.getClient();
        RBatch batch = redissonClient.createBatch();
        batch.getMap("test1").fastPutAsync("1", "2");
        batch.getMap("test2").fastPutAsync("2", "3");
        batch.getMap("test3").putAsync("2", "5");
        RFuture<Long> future = batch.getAtomicLong("counter").incrementAndGetAsync();
        batch.getAtomicLong("counter").incrementAndGetAsync();



        BatchResult<?> res = batch.execute();
        log.info(res.getResponses().toString());
        Long counter = (Long) res.getResponses().get(3);
        log.info(counter.toString());
        log.info(String.valueOf(future.get().equals(counter)));*/


        //连接redis
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        JedisPool jedisPool = new JedisPool(jedisPoolConfig, "192.168.0.60", 6379, 10, "ruoyi123",1);
        Jedis jedis = jedisPool.getResource();


        //获取pipeline
        Pipeline pipeline = jedis.pipelined();

        //原生模式
        String zSetKey = "pipeline-test-";
        int size = 1000;

        Long begin = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            jedis.zadd(zSetKey + i, i, String.valueOf(i));
            jedis.zadd(zSetKey, i + 1, String.valueOf(i + 1));
        }
        log.info("原生模式耗时：{}", (System.currentTimeMillis() - begin));

        //pipeline模式
        begin = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            pipeline.zadd(zSetKey + i, i, String.valueOf(i));
            pipeline.zadd(zSetKey, i + 1, String.valueOf(i + 1));
        }
        pipeline.sync();
        log.info("pipeline模式(Jedis)耗时：{}", (System.currentTimeMillis() - begin));

        //连接redis
        RedissonClient redissonClient = RedisUtils.getClient();
        //pipeline
        RBatch redisBatch = redissonClient.createBatch();


        begin = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            redisBatch.getScoredSortedSet(zSetKey + i).addAsync(i,String.valueOf(i));
            redisBatch.getScoredSortedSet(zSetKey).addAsync(i + 1,String.valueOf(i + 1));
        }
        redisBatch.execute();
        log.info("pipeline模式(Redission)耗时：{}", (System.currentTimeMillis() - begin));

        //关闭
        pipeline.close();
        jedis.close();
        redissonClient.shutdown();
    }
    @DisplayName("测试 @SpringBootTest @Test @DisplayName 注解")
    @Test
    public void testTest() {
        System.out.println(appName);
    }

    @Disabled
    @DisplayName("测试 @Disabled 注解")
    @Test
    public void testDisabled() {
        System.out.println(appName);
    }

    @Timeout(value = 2L, unit = TimeUnit.SECONDS)
    @DisplayName("测试 @Timeout 注解")
    @Test
    public void testTimeout() throws InterruptedException {
        Thread.sleep(3000);
        System.out.println(appName);
    }


    @DisplayName("测试 @RepeatedTest 注解")
    @RepeatedTest(3)
    public void testRepeatedTest() {
        System.out.println(666);
    }

    @BeforeAll
    public static void testBeforeAll() {
        System.out.println("@BeforeAll ==================");
    }

    @BeforeEach
    public void testBeforeEach() {
        System.out.println("@BeforeEach ==================");
    }

    @AfterEach
    public void testAfterEach() {
        System.out.println("@AfterEach ==================");
    }

    @AfterAll
    public static void testAfterAll() {
        System.out.println("@AfterAll ==================");
    }

}
