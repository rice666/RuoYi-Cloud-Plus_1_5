package com.ruoyi.demo.domain;

import lombok.Data;

@Data
public class Person {

    private String name;
    private int age;
}
