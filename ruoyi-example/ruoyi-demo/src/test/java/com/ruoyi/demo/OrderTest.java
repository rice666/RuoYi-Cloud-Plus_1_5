package com.ruoyi.demo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.ruoyi.demo.domain.Order;
import com.ruoyi.demo.domain.User;
import com.ruoyi.demo.mapper.OrderMapper;
import com.ruoyi.demo.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
class OrderTest {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    UserMapper userMapper;

    @Test
    void find() {
        Order order = orderMapper.selectById(1640990702722723841L);
    }

    @Test
    void page() {
        Page<Order> page = new Page<>();
        page.setCurrent(3L);
        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight("goods_name","测试");
        queryWrapper.orderByAsc("id");
        orderMapper.selectPage(page,queryWrapper);
        System.out.println(page.getTotal());
        for(Order order : page.getRecords()){
            System.out.print(order.getGoodsName()+" ");
        }
    }

    @Test
    void insert() {
        for(int i = 1; i <= 100; i++){
            Order order = new Order();
            order.setGoodsName("测试-" + i);
            order.setAmount("11" + i);
            order.setCreateTime(new Date());
//        order.setId(1L);
            orderMapper.insert(order);
//            int i=1/0;
        }

    }
    @Test
    void insertUser() {
        User user = new User();
        user.setId(1L);
        user.setUserName("abc");
        user.setAge(18);
        user.setCreateTime(new Date());
        userMapper.insert(user);
    }

}
