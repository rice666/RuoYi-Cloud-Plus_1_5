package com.ruoyi.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.demo.domain.TOrderItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TOrderItemMapper extends BaseMapper<TOrderItem> {


}
