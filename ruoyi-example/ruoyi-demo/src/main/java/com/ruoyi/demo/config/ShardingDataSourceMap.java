package com.ruoyi.demo.config;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.sql.DataSource;
import java.util.Map;

@Data
@AllArgsConstructor
public class ShardingDataSourceMap {
    private Map<String, DataSource> dataSourceMap;
}
