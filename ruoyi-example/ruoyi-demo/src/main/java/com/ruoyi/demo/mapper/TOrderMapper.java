package com.ruoyi.demo.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.demo.domain.TOrder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TOrderMapper extends BaseMapper<TOrder> {


}
