package com.ruoyi.demo.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.demo.domain.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@DS("tenant-1")
public interface OrderMapper extends BaseMapper<Order> {


}
