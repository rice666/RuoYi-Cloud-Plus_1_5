package com.ruoyi.demo.mapper;


import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.demo.domain.User;
import org.apache.ibatis.annotations.Mapper;

@DS("tenant-2")
@Mapper
public interface UserMapper extends BaseMapper<User> {


}
