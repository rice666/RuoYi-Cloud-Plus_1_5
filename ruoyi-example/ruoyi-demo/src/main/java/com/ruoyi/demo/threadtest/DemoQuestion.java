package com.ruoyi.demo.threadtest;

/**
 * 线程间访问共享变量之间问题
 * */
public class DemoQuestion {
    private String name;

    public static void main(String[] args) {
        DemoQuestion demoQuestion = new DemoQuestion();
        for (int i = 0; i < 5; i++) {
            // int j = i;
            new Thread(() ->{
                demoQuestion.setName(Thread.currentThread().getName() + "的数据");
                System.out.println(Thread.currentThread().getName() + "--->" + demoQuestion.getName());
            },"t" + i).start();
        }
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
