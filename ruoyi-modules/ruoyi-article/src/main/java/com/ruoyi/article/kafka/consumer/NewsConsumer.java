package com.ruoyi.article.kafka.consumer;

import com.ruoyi.article.service.IApArticleConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.function.Consumer;

/**
 * 文章管理消息处理
 */
@Slf4j
@Component
public class NewsConsumer {

    @Autowired
    private IApArticleConfigService apArticleConfigService;
    /**
     * 文章上下架
     */
    @Bean
    Consumer<HashMap> upordown() {

        log.info("初始化订阅upordown");
        return map -> {
            if(map != null){
                apArticleConfigService.updateByMap(map);
                log.info("article端文章配置修改，articleId={}",map.get("articleId"));
            }
        };
    }



}
