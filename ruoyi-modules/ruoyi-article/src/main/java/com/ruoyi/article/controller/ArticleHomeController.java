package com.ruoyi.article.controller;



import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.article.api.domain.ApArticle;
import com.ruoyi.article.api.model.ArticleHomeDto;
import com.ruoyi.article.service.ApArticleService;
import com.ruoyi.common.constants.ArticleConstants;
import com.ruoyi.common.core.domain.R;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/article")
public class ArticleHomeController {

    @Autowired
    private ApArticleService apArticleService;

    @SaCheckPermission("app:all")
    @PostMapping("/load")
    public R<List<ApArticle>> load(@RequestBody ArticleHomeDto dto) {
        return apArticleService.load(ArticleConstants.LOADTYPE_LOAD_MORE,dto);
    }
//    //@SaCheckPermission("app:all")
//    @PostMapping("/loadmore")
//    public ResponseResult loadMore(@RequestBody ArticleHomeDto dto) {
//        return apArticleService.load(ArticleConstants.LOADTYPE_LOAD_MORE,dto);
//    }
//    //@SaCheckPermission("app:all")
//    @PostMapping("/loadnew")
//    public ResponseResult loadNew(@RequestBody ArticleHomeDto dto) {
//        return apArticleService.load(ArticleConstants.LOADTYPE_LOAD_NEW,dto);
//    }
}
