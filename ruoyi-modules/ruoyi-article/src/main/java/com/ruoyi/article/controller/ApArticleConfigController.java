package com.ruoyi.article.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.article.api.domain.bo.ApArticleConfigBo;
import com.ruoyi.article.api.domain.vo.ApArticleConfigVo;
import com.ruoyi.article.service.IApArticleConfigService;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * APP已发布文章配置控制器
 * 前端访问路由地址为:/article/config
 *
 * @author xiaoqiang
 * @date 2022-11-11
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/config")
public class ApArticleConfigController extends BaseController {

    private final IApArticleConfigService iApArticleConfigService;

    /**
     * 查询APP已发布文章配置列表
     */
    @SaCheckPermission("article:config:list")
    @GetMapping("/list")
    public TableDataInfo<ApArticleConfigVo> list(ApArticleConfigBo bo, PageQuery pageQuery) {
        return iApArticleConfigService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出APP已发布文章配置列表
     */
    @SaCheckPermission("article:config:export")
    @Log(title = "APP已发布文章配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ApArticleConfigBo bo, HttpServletResponse response) {
        List<ApArticleConfigVo> list = iApArticleConfigService.queryList(bo);
        ExcelUtil.exportExcel(list, "APP已发布文章配置", ApArticleConfigVo.class, response);
    }

    /**
     * 获取APP已发布文章配置详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("article:config:query")
    @GetMapping("/{id}")
    public R<ApArticleConfigVo> getInfo(@NotNull(message = "主键不能为空") @PathVariable String id) {
        return R.ok(iApArticleConfigService.queryById(id));
    }

    /**
     * 新增APP已发布文章配置
     */
    @SaCheckPermission("article:config:add")
    @Log(title = "APP已发布文章配置", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ApArticleConfigBo bo) {
        return toAjax(iApArticleConfigService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改APP已发布文章配置
     */
    @SaCheckPermission("article:config:edit")
    @Log(title = "APP已发布文章配置", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ApArticleConfigBo bo) {
        return toAjax(iApArticleConfigService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除APP已发布文章配置
     *
     * @param ids 主键串
     */
    @SaCheckPermission("article:config:remove")
    @Log(title = "APP已发布文章配置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空") @PathVariable String[] ids) {
        return toAjax(iApArticleConfigService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
