package com.ruoyi.article.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.article.api.domain.ApArticleConfig;
import com.ruoyi.article.api.domain.bo.ApArticleConfigBo;
import com.ruoyi.article.api.domain.vo.ApArticleConfigVo;
import com.ruoyi.article.mapper.ApArticleConfigMapper;
import com.ruoyi.article.service.IApArticleConfigService;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * APP已发布文章配置Service业务层处理
 *
 * @author xiaoqiang
 * @date 2022-11-11
 */
@RequiredArgsConstructor
@Service
public class ApArticleConfigServiceImpl implements IApArticleConfigService {

    private final ApArticleConfigMapper baseMapper;

    /**
     * 查询APP已发布文章配置
     */
    @Override
    public ApArticleConfigVo queryById(String id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询APP已发布文章配置列表
     */
    @Override
    public TableDataInfo<ApArticleConfigVo> queryPageList(ApArticleConfigBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<ApArticleConfig> lqw = buildQueryWrapper(bo);
        Page<ApArticleConfigVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询APP已发布文章配置列表
     */
    @Override
    public List<ApArticleConfigVo> queryList(ApArticleConfigBo bo) {
        LambdaQueryWrapper<ApArticleConfig> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<ApArticleConfig> buildQueryWrapper(ApArticleConfigBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<ApArticleConfig> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getArticleId()!=null, ApArticleConfig::getArticleId, bo.getArticleId());
        lqw.eq(StringUtils.isNotBlank(bo.getIsComment()), ApArticleConfig::getIsComment, bo.getIsComment());
        lqw.eq(StringUtils.isNotBlank(bo.getIsForward()), ApArticleConfig::getIsForward, bo.getIsForward());
        lqw.eq(StringUtils.isNotBlank(bo.getIsDown()), ApArticleConfig::getIsDown, bo.getIsDown());
        lqw.eq(StringUtils.isNotBlank(bo.getIsDelete()), ApArticleConfig::getIsDelete, bo.getIsDelete());
        return lqw;
    }

    /**
     * 新增APP已发布文章配置
     */
    @Override
    public Boolean insertByBo(ApArticleConfigBo bo) {
        ApArticleConfig add = BeanUtil.toBean(bo, ApArticleConfig.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改APP已发布文章配置
     */
    @Override
    public Boolean updateByBo(ApArticleConfigBo bo) {
        ApArticleConfig update = BeanUtil.toBean(bo, ApArticleConfig.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(ApArticleConfig entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除APP已发布文章配置
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 修改文章配置
     * @param map
     */
    @Override
    public void updateByMap(Map map) {
        //0 下架 1 上架
        Object enable = map.get("enable");
        boolean isDown = true;
        if(enable.equals(1)){
            isDown = false;
        }
        //修改文章配置
        LambdaQueryWrapper<ApArticleConfig> lqw = Wrappers.lambdaQuery();
        lqw.eq(ApArticleConfig::getArticleId, map.get("articleId"));

        ApArticleConfig apArticleConfig = baseMapper.selectOne(lqw);

        apArticleConfig.setIsDown(isDown);
        baseMapper.updateById(apArticleConfig);
    }
}
