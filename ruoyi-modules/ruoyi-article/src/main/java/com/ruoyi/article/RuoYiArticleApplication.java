package com.ruoyi.article;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;

/**
 * 系统模块
 *
 * @author ruoyi
 */
@EnableDubbo
@SpringBootApplication(scanBasePackages = {"com.ruoyi.article.*","com.ruoyi.article.api.*"})

public class RuoYiArticleApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(RuoYiArticleApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Article模块启动成功   ლ(´ڡ`ლ)ﾞ  ");
    }
}
