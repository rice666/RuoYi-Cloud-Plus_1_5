package com.ruoyi.article.mapper;


import com.ruoyi.article.api.domain.ApArticleContent;
import com.ruoyi.article.api.domain.vo.ApArticleContentVo;
import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;

public interface ApArticleContentMapper extends BaseMapperPlus<ApArticleContentMapper, ApArticleContent, ApArticleContentVo> {
}
