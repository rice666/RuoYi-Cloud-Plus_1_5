package com.ruoyi.article.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ruoyi.article.api.domain.ApArticle;
import com.ruoyi.article.api.domain.ApArticleConfig;
import com.ruoyi.article.api.domain.ApArticleContent;
import com.ruoyi.article.api.model.ArticleDto;
import com.ruoyi.article.api.model.ArticleHomeDto;
import com.ruoyi.article.mapper.ApArticleConfigMapper;
import com.ruoyi.article.mapper.ApArticleContentMapper;
import com.ruoyi.article.mapper.ApArticleMapper;
import com.ruoyi.article.service.ApArticleService;
import com.ruoyi.article.service.ArticleFreemarkerService;
import com.ruoyi.common.constants.ArticleConstants;
import com.ruoyi.common.core.domain.R;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@RequiredArgsConstructor
@Service
public class ApArticleServiceImpl implements ApArticleService {

    // 单页最大加载的数字
    private final static short MAX_PAGE_SIZE = 50;


    private final ApArticleMapper apArticleMapper;

    /**
     * 根据参数加载文章列表
     * @param loadtype 1为加载更多  2为加载最新
     * @param dto
     * @return
     */
    @Override
    public R<List<ApArticle>> load(Short loadtype, ArticleHomeDto dto) {
//        //1.校验参数
//        Integer size = dto.getSize();
//        if(size == null || size == 0){
//            size = 10;
//        }
//        size = Math.min(size,MAX_PAGE_SIZE);
//        dto.setSize(size);
//
//        //类型参数检验
//        if(!loadtype.equals(ArticleConstants.LOADTYPE_LOAD_MORE)&&!loadtype.equals(ArticleConstants.LOADTYPE_LOAD_NEW)){
//            loadtype = ArticleConstants.LOADTYPE_LOAD_MORE;
//        }
//        //文章频道校验
//        if(StringUtils.isEmpty(dto.getTag())){
//            dto.setTag(ArticleConstants.DEFAULT_TAG);
//        }
//
//        //时间校验
//        if(dto.getMaxBehotTime() == null) dto.setMaxBehotTime(new Date());
//        if(dto.getMinBehotTime() == null) dto.setMinBehotTime(new Date());
        //2.查询数据
        List<ApArticle> apArticles = apArticleMapper.selectList();

        //3.结果封装
        return R.ok(apArticles);
    }


    private final ApArticleConfigMapper apArticleConfigMapper;


    private final ApArticleContentMapper apArticleContentMapper;

    private  final ArticleFreemarkerService articleFreemarkerService;

    /**
     * 保存自媒体文章到文章库
     * @param dto
     * @return
     */
    @Override
    public R saveArticle(ArticleDto dto) {
        //todo 1.校验参数
        if(dto == null){
            //return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
            R.fail("保存文章出现错误！");
        }
        //todo 2.判断是否存在文章ID
        if(dto.getId() == null){
            //todo 2.1 如果不存在，代表新增
            //todo 2.1.1 保存数据到文章表
            apArticleMapper.insert(dto);
            //todo 2.1.2 保存数据到文章配置表
            ApArticleConfig apArticleConfig = new ApArticleConfig(dto.getId());
            apArticleConfigMapper.insert(apArticleConfig);
            //todo 2.1.3 保存数据到文章内容表
            ApArticleContent apArticleContent = new ApArticleContent();
            apArticleContent.setContent(dto.getContent());
            apArticleContent.setArticleId(dto.getId());
            apArticleContentMapper.insert(apArticleContent);
        }else{
            //todo 2.2 如果存在，代表修改
            //todo 2.2.1 修改文章表数据
            apArticleMapper.updateById(dto);
            //todo 2.2.2 修改文章内容表数据
            LambdaQueryWrapper<ApArticleContent> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(ApArticleContent::getArticleId,dto.getId());
            ApArticleContent apArticleContent = apArticleContentMapper.selectOne(wrapper);
            apArticleContent.setContent(dto.getContent());
            apArticleContentMapper.updateById(apArticleContent);
        }

        //todo 3.根据文章内容，来生成静态页面并且存储到minio中
        try {
            articleFreemarkerService.buildArticleToMinIO(dto,dto.getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }


        return R.ok(dto.getId());
    }
}
