package com.ruoyi.article.service;


import com.ruoyi.article.api.domain.ApArticle;
import com.ruoyi.article.api.model.ArticleDto;
import com.ruoyi.article.api.model.ArticleHomeDto;
import com.ruoyi.common.core.domain.R;

import java.util.List;

public interface ApArticleService{

    /**
     * 根据参数加载文章列表
     * @param loadtype 1为加载更多  2为加载最新
     * @param dto
     * @return
     */
    R<List<ApArticle>> load(Short loadtype, ArticleHomeDto dto);

    public R<Void> saveArticle(ArticleDto dto);

}
