package com.ruoyi.article.service;


import com.ruoyi.article.api.domain.bo.ApArticleConfigBo;
import com.ruoyi.article.api.domain.vo.ApArticleConfigVo;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * APP已发布文章配置Service接口
 *
 * @author xiaoqiang
 * @date 2022-11-11
 */
public interface IApArticleConfigService {

    /**
     * 查询APP已发布文章配置
     */
    ApArticleConfigVo queryById(String id);

    /**
     * 查询APP已发布文章配置列表
     */
    TableDataInfo<ApArticleConfigVo> queryPageList(ApArticleConfigBo bo, PageQuery pageQuery);

    /**
     * 查询APP已发布文章配置列表
     */
    List<ApArticleConfigVo> queryList(ApArticleConfigBo bo);

    /**
     * 修改APP已发布文章配置
     */
    Boolean insertByBo(ApArticleConfigBo bo);

    /**
     * 修改APP已发布文章配置
     */
    Boolean updateByBo(ApArticleConfigBo bo);

    /**
     * 校验并批量删除APP已发布文章配置信息
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    /**
     * 修改文章配置
     * @param map
     */
    public void updateByMap(Map map);
}
