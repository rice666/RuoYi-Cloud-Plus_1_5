package com.ruoyi.article.mapper;

import com.ruoyi.article.api.domain.ApArticleConfig;
import com.ruoyi.article.api.domain.vo.ApArticleConfigVo;
import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;

/**
 * APP已发布文章配置Mapper接口
 *
 * @author xiaoqiang
 * @date 2022-10-30
 */
public interface ApArticleConfigMapper extends BaseMapperPlus<ApArticleConfigMapper, ApArticleConfig, ApArticleConfigVo> {

}
