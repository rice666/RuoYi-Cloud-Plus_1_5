package com.ruoyi.article.mapper;


import com.ruoyi.article.api.domain.ApArticle;
import com.ruoyi.article.api.domain.vo.ApArticleVo;
import com.ruoyi.article.api.model.ArticleHomeDto;
import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ApArticleMapper extends BaseMapperPlus<ApArticleMapper, ApArticle, ApArticleVo> {
    public List<ApArticle> loadArticleList(@Param("dto") ArticleHomeDto dto, @Param("type") Short type);
}
