package com.ruoyi.article.kafka.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.function.Consumer;
/**
 * 测试用
 */
@Slf4j
@Component
public class TestStreamConsumer {

    @Bean
    Consumer<HashMap> test() {
        log.info("初始化订阅test");
        return msg -> {
            log.info("test通过stream消费到消息 => {}", msg.toString());
        };
    }





}
