package com.ruoyi.article.kafka.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.function.Consumer;

/**
 * 测试用
 */
@Slf4j
@Component
public class LogStreamConsumer {


    @Bean
    Consumer<HashMap> log() {

        log.info("初始化订阅log");
        return msg -> {
            log.info("log通过stream消费到消息 => {}", msg.toString());
        };
    }



}
