package com.ruoyi.article.dubbo;

import com.ruoyi.article.api.IArticleClient;
import com.ruoyi.article.api.model.ArticleDto;
import com.ruoyi.article.service.ApArticleService;
import com.ruoyi.common.core.domain.R;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@DubboService
public class RemoteApArticleService implements IArticleClient {

    private final ApArticleService apArticleService;
    @Override
    public R saveArticle(ArticleDto dto) {
        System.out.println("服务调用IArticleClient.saveArticle:" + dto);
        return apArticleService.saveArticle(dto);
    }
}
