package com.ruoyi.article.kafka.producer;

//import com.ruoyi.search.domain.SearchArticleVo;
import com.ruoyi.search.domain.SearchArticleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;


/**
 * 文章es索引处理
 */
@Component
public class ArticleESIndexStreamProducer {

    @Autowired
    private StreamBridge streamBridge;

    public void createArticleESIndex(SearchArticleVo vo) {
        if(vo != null){
            System.out.println("发送索引消息到search");
            streamBridge.send("articleindex-out-0", MessageBuilder.withPayload(vo).build());
        }


    }
}
