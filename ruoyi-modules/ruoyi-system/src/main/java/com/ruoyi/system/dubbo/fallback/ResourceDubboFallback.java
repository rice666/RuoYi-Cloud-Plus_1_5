package com.ruoyi.system.dubbo.fallback;

import com.alibaba.csp.sentinel.adapter.dubbo3.fallback.DubboFallback;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ruoyi.common.core.exception.ServiceException;
import org.apache.dubbo.rpc.Invocation;
import org.apache.dubbo.rpc.Invoker;
import org.apache.dubbo.rpc.Result;

public class ResourceDubboFallback implements DubboFallback {
    @Override
    public Result handle(Invoker<?> invoker, Invocation invocation, BlockException ex) {
        System.out.println("进入resource熔断方法!");
        throw new ServiceException("资源服务器忙，请稍后再试！");
    }
}
