package com.ruoyi.wemedia.service;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.wemedia.domain.bo.NewsBo;
import com.ruoyi.wemedia.domain.vo.NewsVo;
import org.apache.poi.ss.formula.functions.T;

import java.util.Collection;
import java.util.List;

/**
 * 文章列表Service接口
 *
 * @author xiaoqiang
 * @date 2022-10-15
 */
public interface INewsService {

    /**
     * 查询文章列表
     */
    NewsVo queryById(Long id);

    /**
     * 查询文章列表列表
     */
    TableDataInfo<NewsVo> queryPageList(NewsBo bo, PageQuery pageQuery);

    /**
     * 查询文章列表列表
     */
    List<NewsVo> queryList(NewsBo bo);

    /**
     * 修改文章列表
     */
    R<T> insertByBo(NewsBo bo);

    /**
     * 修改文章列表
     */
    Boolean updateByBo(NewsBo bo);

    /**
     * 校验并批量删除文章列表信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 文章的上下架
     *
     *
     */
    R<T> downOrUp(NewsBo bo);
}
