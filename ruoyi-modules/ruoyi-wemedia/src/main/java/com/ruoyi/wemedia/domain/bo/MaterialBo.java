package com.ruoyi.wemedia.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 自媒体图文素材信息业务对象
 *
 * @author xiaoqiang
 * @date 2023-02-25
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class MaterialBo extends BaseEntity {

    /**
     * 主键
     */

    private Long id;

    /**
     * 图片地址
     */
    private String url;

    /**
     * 素材类型
            0 图片
            1 视频
     */
    private String type;

    /**
     * 是否收藏
     */
    private Integer isCollection;


}
