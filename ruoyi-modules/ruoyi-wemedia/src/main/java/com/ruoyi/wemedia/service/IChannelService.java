package com.ruoyi.wemedia.service;

import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.wemedia.domain.bo.ChannelBo;
import com.ruoyi.wemedia.domain.vo.ChannelVo;

import java.util.Collection;
import java.util.List;

/**
 * 频道信息Service接口
 *
 * @author xiaoqiang
 * @date 2022-10-14
 */
public interface IChannelService {

    /**
     * 查询频道信息
     */
    ChannelVo queryById(Integer id);

    /**
     * 查询频道信息列表
     */
    TableDataInfo<ChannelVo> queryPageList(ChannelBo bo, PageQuery pageQuery);

    /**
     * 查询频道信息列表
     */
    List<ChannelVo> queryList(ChannelBo bo);

    /**
     * 修改频道信息
     */
    Boolean insertByBo(ChannelBo bo);

    /**
     * 修改频道信息
     */
    Boolean updateByBo(ChannelBo bo);

    /**
     * 校验并批量删除频道信息信息
     */
    Boolean deleteWithValidByIds(Collection<Integer> ids, Boolean isValid);
}
