package com.ruoyi.wemedia.service;

import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.wemedia.domain.bo.NewsMaterialBo;
import com.ruoyi.wemedia.domain.vo.NewsMaterialVo;

import java.util.Collection;
import java.util.List;

/**
 * 自媒体图文引用素材信息Service接口
 *
 * @author xiaoqiang
 * @date 2022-10-18
 */
public interface INewsMaterialService {

    /**
     * 查询自媒体图文引用素材信息
     */
    NewsMaterialVo queryById(Long id);

    /**
     * 查询自媒体图文引用素材信息列表
     */
    TableDataInfo<NewsMaterialVo> queryPageList(NewsMaterialBo bo, PageQuery pageQuery);

    /**
     * 查询自媒体图文引用素材信息列表
     */
    List<NewsMaterialVo> queryList(NewsMaterialBo bo);

    /**
     * 修改自媒体图文引用素材信息
     */
    Boolean insertByBo(NewsMaterialBo bo);

    /**
     * 修改自媒体图文引用素材信息
     */
    Boolean updateByBo(NewsMaterialBo bo);

    /**
     * 校验并批量删除自媒体图文引用素材信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
