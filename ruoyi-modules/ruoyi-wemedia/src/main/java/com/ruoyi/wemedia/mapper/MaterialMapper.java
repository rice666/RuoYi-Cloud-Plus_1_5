package com.ruoyi.wemedia.mapper;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.wemedia.domain.Material;
import com.ruoyi.wemedia.domain.vo.MaterialVo;

/**
 * 自媒体图文素材信息Mapper接口
 *
 * @author xiaoqiang
 * @date 2023-02-25
 */
public interface MaterialMapper extends BaseMapperPlus<MaterialMapper, Material, MaterialVo> {

}
