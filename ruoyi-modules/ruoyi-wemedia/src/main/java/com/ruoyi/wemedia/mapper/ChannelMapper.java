package com.ruoyi.wemedia.mapper;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.wemedia.domain.Channel;
import com.ruoyi.wemedia.domain.vo.ChannelVo;

/**
 * 频道信息Mapper接口
 *
 * @author xiaoqiang
 * @date 2022-10-14
 */
public interface ChannelMapper extends BaseMapperPlus<ChannelMapper, Channel, ChannelVo> {

}
