package com.ruoyi.wemedia.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * 自媒体图文引用素材信息业务对象
 *
 * @author xiaoqiang
 * @date 2022-10-18
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class NewsMaterialBo extends BaseEntity {

    /**
     * 主键
     */
    private Long id;

    /**
     * 素材ID
     */
    private Long materialId;

    /**
     * 图文ID
     */
    private Long newsId;

    /**
     * 引用类型
            0 内容引用
            1 主图引用
     */

    private Integer type;

    /**
     * 引用排序
     */
    private Integer ord;


}
