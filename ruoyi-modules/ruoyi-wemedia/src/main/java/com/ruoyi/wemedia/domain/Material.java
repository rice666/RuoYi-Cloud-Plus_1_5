package com.ruoyi.wemedia.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 自媒体图文素材信息对象 wm_material
 *
 * @author xiaoqiang
 * @date 2023-02-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wm_material")
public class Material extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 图片地址
     */
    private String url;
    /**
     * 素材类型
            0 图片
            1 视频
     */
    private String type;
    /**
     * 是否收藏
     */
    private Integer isCollection;

}
