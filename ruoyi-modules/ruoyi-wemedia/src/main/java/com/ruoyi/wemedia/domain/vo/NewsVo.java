package com.ruoyi.wemedia.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.util.Date;


/**
 * 文章列表视图对象
 *
 * @author xiaoqiang
 * @date 2022-10-15
 */
@Data
@ExcelIgnoreUnannotated
public class NewsVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 自媒体用户ID
     */
    @ExcelProperty(value = "自媒体用户ID")
    private Long userId;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String title;

    /**
     * 图文内容
     */
    @ExcelProperty(value = "图文内容")
    private String content;

    /**
     * 文章布局
            0 无图文章
            1 单图文章
            3 多图文章
     */
    @ExcelProperty(value = "文章布局")
    private Short type;

    /**
     * 图文频道ID
     */
    @ExcelProperty(value = "图文频道ID")
    private Long channelId;

    /**
     * 标签
     */
    @ExcelProperty(value = "标签")
    private String labels;

    /**
     * 提交时间
     */
    @ExcelProperty(value = "提交时间")
    private Date submitedTime;

    /**
     * 当前状态
            0 草稿
            1 提交（待审核）
            2 审核失败
            3 人工审核
            4 人工审核通过
            8 审核通过（待发布）
            9 已发布
     */
    private Short status;

    /**
     * 定时发布时间，不定时则为空
     */
    @ExcelProperty(value = "定时发布时间，不定时则为空")
    private Date publishTime;

    /**
     * 拒绝理由
     */
    @ExcelProperty(value = "拒绝理由")
    private String reason;

    /**
     * 发布库文章ID
     */
    @ExcelProperty(value = "发布库文章ID")
    private String articleId;

    /**
     * 图片
     */
    @ExcelProperty(value = "图片")
    private String images;

    /**
     * 上下架 0 下架  1 上架
     */
    @ExcelProperty(value = "上架")
    private Short enable;


    @ExcelProperty(value = "创建日期")
    private Date createTime;


}
