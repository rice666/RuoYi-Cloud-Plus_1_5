package com.ruoyi.wemedia.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;

import com.ruoyi.wemedia.domain.bo.NewsBo;
import com.ruoyi.wemedia.domain.bo.NewsMaterialBo;
import com.ruoyi.wemedia.domain.vo.NewsMaterialVo;
import com.ruoyi.wemedia.domain.vo.NewsVo;

import com.ruoyi.wemedia.service.INewsMaterialService;
import com.ruoyi.wemedia.service.INewsService;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 文章列表控制器
 * 前端访问路由地址为:/wemedia/news
 *
 * @author xiaoqiang
 * @date 2022-10-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/news")
public class NewsController extends BaseController {

    private final INewsService iNewsService;

    private final INewsMaterialService iNewsMaterialService;

    /**
     * 查询文章列表列表
     */
    @SaCheckPermission("wemedia:news:list")
    @GetMapping("/list")
    public TableDataInfo<NewsVo> list(NewsBo bo, PageQuery pageQuery) {
        return iNewsService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出文章列表列表
     */
    @SaCheckPermission("wemedia:news:export")
    @Log(title = "文章列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(NewsBo bo, HttpServletResponse response) {
        List<NewsVo> list = iNewsService.queryList(bo);
        ExcelUtil.exportExcel(list, "文章列表", NewsVo.class, response);
    }

    /**
     * 获取文章列表详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wemedia:news:query")
    @GetMapping("/{id}")
    public R<NewsVo> getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return R.ok(iNewsService.queryById(id));
    }

    /**
     * 新增文章列表
     */
    @SaCheckPermission("wemedia:news:add")
    @Log(title = "文章列表", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<T> add(@Validated(AddGroup.class) @RequestBody NewsBo bo) {
        return iNewsService.insertByBo(bo);
    }

    /**
     * 修改文章列表
     */
    @SaCheckPermission("wemedia:news:edit")
    @Log(title = "文章列表", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody NewsBo bo) {
        return toAjax(iNewsService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除文章列表
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wemedia:news:remove")
    @Log(title = "文章列表", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        //需要先删除素材关系
        for(Long newsId : ids){
            NewsMaterialBo newsMaterialBo = new NewsMaterialBo();
            newsMaterialBo.setNewsId(newsId);
            List<NewsMaterialVo> newsMaterialVos = iNewsMaterialService.queryList(newsMaterialBo);
            List<Long> nmIds = new ArrayList<>();
            for(NewsMaterialVo nmVo : newsMaterialVos){
                nmIds.add(nmVo.getId());
            }
            iNewsMaterialService.deleteWithValidByIds(nmIds,true);
        }
        return toAjax(iNewsService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

}
