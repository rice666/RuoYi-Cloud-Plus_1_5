package com.ruoyi.wemedia.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 文章列表业务对象
 *
 * @author xiaoqiang
 * @date 2022-10-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class NewsBo extends BaseEntity {

    private Long id;
    /**
     * 自媒体用户ID
     */
    //@NotNull(message = "自媒体用户ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String title;

    /**
     * 图文内容
     */
    @NotBlank(message = "图文内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String content;

    /**
     * 文章布局
            0 无图文章
            1 单图文章
            3 多图文章
     */
    @NotNull(message = "文章布局", groups = { AddGroup.class, EditGroup.class })
    private Short type;

    /**
     * 图文频道ID
     */
    //@NotNull(message = "图文频道ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long channelId;

    /**
     * 标签
     */
    //@NotBlank(message = "标签不能为空", groups = { AddGroup.class, EditGroup.class })
    private String labels;

    /**
     * 提交时间
     */
    //@NotNull(message = "提交时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date submitedTime;

    /**
     * 当前状态
            0 草稿
            1 提交（待审核）
            2 审核失败
            3 人工审核
            4 人工审核通过
            8 审核通过（待发布）
            9 已发布
     */
    //@NotBlank(message = "当前状态", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 定时发布时间，不定时则为空
     */
    //@NotNull(message = "定时发布时间，不定时则为空不能为空", groups = { AddGroup.class, EditGroup.class })
    //@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date publishTime;

    /**
     * 拒绝理由
     */
    //@NotBlank(message = "拒绝理由不能为空", groups = { AddGroup.class, EditGroup.class })
    private String reason;

    /**
     * 发布库文章ID
     */
    //@NotBlank(message = "发布库文章ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String articleId;

    /**
     * 图片
     */
    //@NotBlank(message = "图片不能为空", groups = { AddGroup.class, EditGroup.class })
    private String images;

    /**
     * 上下架 0 下架  1 上架
     */
    //@NotBlank(message = "上架不能为空", groups = { AddGroup.class, EditGroup.class })
    private Short enable;

    /**
     * 开始时间
     */

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginPubDate;

    /**
     * 结束时间
     */

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endPubDate;


}
