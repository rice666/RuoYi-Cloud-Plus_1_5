package com.ruoyi.wemedia.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.excel.annotation.ExcelDictFormat;
import com.ruoyi.common.excel.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 自媒体图文素材信息视图对象
 *
 * @author xiaoqiang
 * @date 2023-02-25
 */
@Data
@ExcelIgnoreUnannotated
public class MaterialVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 图片地址
     */
    @ExcelProperty(value = "图片地址")
    private String url;

    /**
     * 素材类型
            0 图片
            1 视频
     */

    private String type;

    /**
     * 是否收藏
     */
    @ExcelProperty(value = "是否收藏")
    private Integer isCollection;


}
