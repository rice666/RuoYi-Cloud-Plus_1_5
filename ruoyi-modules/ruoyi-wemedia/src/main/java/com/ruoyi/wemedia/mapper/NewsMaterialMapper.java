package com.ruoyi.wemedia.mapper;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.wemedia.domain.NewsMaterial;

import com.ruoyi.wemedia.domain.vo.NewsMaterialVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 自媒体图文引用素材信息Mapper接口
 *
 * @author xiaoqiang
 * @date 2022-10-18
 */
public interface NewsMaterialMapper extends BaseMapperPlus<NewsMaterialMapper, NewsMaterial, NewsMaterialVo> {
    void saveRelations(@Param("materialIds") List<Long> materialIds, @Param("newsId") Long newsId, @Param("type")Short type);
}
