package com.ruoyi.wemedia.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 自媒体图文引用素材信息视图对象
 *
 * @author xiaoqiang
 * @date 2022-10-18
 */
@Data
@ExcelIgnoreUnannotated
public class NewsMaterialVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 素材ID
     */
    @ExcelProperty(value = "素材ID")
    private Long materialId;

    /**
     * 图文ID
     */
    @ExcelProperty(value = "图文ID")
    private Long newsId;

    /**
     * 引用类型
            0 内容引用
            1 主图引用
     */

    private Integer type;

    /**
     * 引用排序
     */
    @ExcelProperty(value = "引用排序")
    private Integer ord;


}
