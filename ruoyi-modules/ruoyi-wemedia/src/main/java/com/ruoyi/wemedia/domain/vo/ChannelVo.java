package com.ruoyi.wemedia.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


/**
 * 频道信息视图对象
 *
 * @author xiaoqiang
 * @date 2022-10-14
 */
@Data
@ExcelIgnoreUnannotated
public class ChannelVo {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ExcelProperty(value = "id")
    private Long id;

    /**
     * 频道名称
     */
    @ExcelProperty(value = "频道名称")
    private String name;

    /**
     * 频道描述
     */
    @ExcelProperty(value = "频道描述")
    private String description;

    /**
     * 是否默认频道
     */
    @ExcelProperty(value = "是否默认频道")
    private Integer isDefault;

    /**
     *
     */
    @ExcelProperty(value = "")
    private Integer status;

    /**
     * 默认排序
     */
    @ExcelProperty(value = "默认排序")
    private Integer ord;


}
