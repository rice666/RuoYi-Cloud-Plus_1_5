package com.ruoyi.wemedia.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.wemedia.domain.bo.ChannelBo;
import com.ruoyi.wemedia.domain.vo.ChannelVo;
import com.ruoyi.wemedia.service.IChannelService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * 频道信息控制器
 * 前端访问路由地址为:/channel/channel
 *
 * @author xiaoqiang
 * @date 2022-10-14
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/channel")
public class ChannelController extends BaseController {

    private final IChannelService iChannelService;

    /**
     * 查询频道信息列表
     */
    @SaCheckPermission("channel:channel:list")
    @GetMapping("/list")
    public TableDataInfo<ChannelVo> list(ChannelBo bo, PageQuery pageQuery) {
        return iChannelService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询频道信息列表
     */

    @GetMapping("/channellist")
    public R<List<ChannelVo>> channellist(ChannelBo bo) {
        List<ChannelVo> list = iChannelService.queryList(bo);
        return  R.ok(list);
    }

    /**
     * 导出频道信息列表
     */
    @SaCheckPermission("channel:channel:export")
    @Log(title = "频道信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ChannelBo bo, HttpServletResponse response) {
        List<ChannelVo> list = iChannelService.queryList(bo);
        ExcelUtil.exportExcel(list, "频道信息", ChannelVo.class, response);
    }

    /**
     * 获取频道信息详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("channel:channel:query")
    @GetMapping("/{id}")
    public R<ChannelVo> getInfo(@NotNull(message = "主键不能为空") @PathVariable Integer id) {
        return R.ok(iChannelService.queryById(id));
    }

    /**
     * 新增频道信息
     */
    @SaCheckPermission("channel:channel:add")
    @Log(title = "频道信息", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ChannelBo bo) {
        return toAjax(iChannelService.insertByBo(bo) ? 1 : 0);
    }

    /**
     * 修改频道信息
     */
    @SaCheckPermission("channel:channel:edit")
    @Log(title = "频道信息", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ChannelBo bo) {
        return toAjax(iChannelService.updateByBo(bo) ? 1 : 0);
    }

    /**
     * 删除频道信息
     *
     * @param ids 主键串
     */
    @SaCheckPermission("channel:channel:remove")
    @Log(title = "频道信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空") @PathVariable Integer[] ids) {
        return toAjax(iChannelService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
