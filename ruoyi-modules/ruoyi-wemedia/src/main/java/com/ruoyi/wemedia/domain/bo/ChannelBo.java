package com.ruoyi.wemedia.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 频道信息业务对象
 *
 * @author xiaoqiang
 * @date 2022-10-14
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ChannelBo extends BaseEntity {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 频道名称
     */
    @NotBlank(message = "频道名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 频道描述
     */
    @NotBlank(message = "频道描述不能为空", groups = { AddGroup.class, EditGroup.class })
    private String description;

    /**
     * 是否默认频道
     */
    @NotNull(message = "是否默认频道不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer isDefault;

    /**
     *
     */
    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * 默认排序
     */
    @NotNull(message = "默认排序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer ord;


}
