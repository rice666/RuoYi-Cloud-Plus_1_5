package com.ruoyi.wemedia.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.common.oss.core.OssClient;
import com.ruoyi.common.oss.entity.UploadResult;
import com.ruoyi.common.oss.factory.OssFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.ruoyi.wemedia.domain.bo.MaterialBo;
import com.ruoyi.wemedia.domain.vo.MaterialVo;
import com.ruoyi.wemedia.domain.Material;
import com.ruoyi.wemedia.mapper.MaterialMapper;
import com.ruoyi.wemedia.service.IMaterialService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 自媒体图文素材信息Service业务层处理
 *
 * @author xiaoqiang
 * @date 2023-02-25
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class MaterialServiceImpl implements IMaterialService {

    private final MaterialMapper baseMapper;

    /**
     * 查询自媒体图文素材信息
     */
    @Override
    public MaterialVo queryById(String id) {
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询自媒体图文素材信息列表
     */
    @Override
    public TableDataInfo<MaterialVo> queryPageList(MaterialBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Material> lqw = buildQueryWrapper(bo);
        Page<MaterialVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询自媒体图文素材信息列表
     */
    @Override
    public List<MaterialVo> queryList(MaterialBo bo) {
        LambdaQueryWrapper<Material> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Material> buildQueryWrapper(MaterialBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Material> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getUrl()), Material::getUrl, bo.getUrl());
        lqw.eq(StringUtils.isNotBlank(bo.getType()), Material::getType, bo.getType());
        lqw.eq(bo.getIsCollection() != null, Material::getIsCollection, bo.getIsCollection());
        return lqw;
    }

    /**
     * 新增自媒体图文素材信息
     */
    @Override
    public Boolean insertByBo(MaterialBo bo) {
        Material add = BeanUtil.toBean(bo, Material.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改自媒体图文素材信息
     */
    @Override
    public Boolean updateByBo(MaterialBo bo) {
        Material update = BeanUtil.toBean(bo, Material.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Material entity) {
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除自媒体图文素材信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {

        log.info("oss中删除内容");
        List<Material> list = baseMapper.selectBatchIds(ids);
        for (Material sysOss : list) {
            OssClient storage = OssFactory.instance("minio");
            storage.delete(sysOss.getUrl());
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public Boolean upload(MultipartFile file) {
        String originalfileName = file.getOriginalFilename();//得到上传时的文件名 xxx.jpg
        String suffix = StringUtils.substring(originalfileName, originalfileName.lastIndexOf("."), originalfileName.length());//xxx
        OssClient storage = OssFactory.instance();
        UploadResult uploadResult;
        try {
            uploadResult = storage.uploadSuffix(file.getBytes(), suffix, file.getContentType());
        } catch (IOException e) {
            throw new ServiceException(e.getMessage());
        }
        Material bo = new Material();
        bo.setUrl(uploadResult.getUrl());
        bo.setType("0");
        bo.setIsCollection(0);
        boolean flag = baseMapper.insert(bo) > 0;
        return flag;
    }
}
