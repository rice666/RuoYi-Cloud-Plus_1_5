package com.ruoyi.wemedia.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class LogStreamProducer {

    @Autowired
    private StreamBridge streamBridge;

    public void streamLogMsg(String msg) {
        // 构建消息对象
        Map testMessaging = new HashMap();
        testMessaging.put("id",UUID.randomUUID().toString());
        testMessaging.put("text","Hello kafka!");
        streamBridge.send("log-out-0", MessageBuilder.withPayload(testMessaging).build());
    }

    public void streamLogMsg1() {
        // 构建消息对象
        Map testMessaging = new HashMap();
        testMessaging.put("id",UUID.randomUUID().toString());
        testMessaging.put("text","hello test stream!");
        streamBridge.send("test-out-1", MessageBuilder.withPayload(testMessaging).build());
    }
}
