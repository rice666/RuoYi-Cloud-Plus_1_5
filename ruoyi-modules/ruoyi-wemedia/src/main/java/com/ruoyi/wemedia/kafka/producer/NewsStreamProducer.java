package com.ruoyi.wemedia.kafka.producer;

import com.ruoyi.wemedia.domain.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 文章管理消息处理
 */
@Component
public class NewsStreamProducer {

    @Autowired
    private StreamBridge streamBridge;

    public void upordown(News wmNews) {
        if(wmNews.getArticleId() != null){
            Map<String,Object> map = new HashMap<>();
            map.put("articleId",wmNews.getArticleId());
            map.put("enable",wmNews.getEnable());
            streamBridge.send("upordown-out-0", MessageBuilder.withPayload(map).build());
        }


    }
}
