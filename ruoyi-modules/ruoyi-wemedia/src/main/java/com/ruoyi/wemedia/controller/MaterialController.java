package com.ruoyi.wemedia.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.exception.ServiceException;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.excel.utils.ExcelUtil;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.wemedia.domain.vo.MaterialVo;
import com.ruoyi.wemedia.domain.bo.MaterialBo;
import com.ruoyi.wemedia.service.IMaterialService;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import java.util.Map;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.servlet.http.HttpServletResponse;

/**
 * 自媒体图文素材信息控制器
 * 前端访问路由地址为:/wemedia/material
 *
 * @author xiaoqiang
 * @date 2023-02-25
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/material")
public class MaterialController extends BaseController {

    private final IMaterialService iMaterialService;

    /**
     * 查询自媒体图文素材信息列表
     */
    @SaCheckPermission("wemedia:material:list")
    @GetMapping("/list")
    public TableDataInfo<MaterialVo> list(MaterialBo bo, PageQuery pageQuery) {
        return iMaterialService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出自媒体图文素材信息列表
     */
    @SaCheckPermission("wemedia:material:export")
    @Log(title = "自媒体图文素材信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(MaterialBo bo, HttpServletResponse response) {
        List<MaterialVo> list = iMaterialService.queryList(bo);
        ExcelUtil.exportExcel(list, "自媒体图文素材信息", MaterialVo.class, response);
    }

    /**
     * 获取自媒体图文素材信息详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wemedia:material:query")
    @GetMapping("/{id}")
    public R<MaterialVo> getInfo(@NotNull(message = "主键不能为空") @PathVariable String id) {
        return R.ok(iMaterialService.queryById(id));
    }

    /**
     * 新增自媒体图文素材信息
     */
    @SaCheckPermission("wemedia:material:add")
    @Log(title = "自媒体图文素材信息", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody MaterialBo bo) {
        return toAjax(iMaterialService.insertByBo(bo));
    }

    /**
     * 修改自媒体图文素材信息
     */
    @SaCheckPermission("wemedia:material:edit")
    @Log(title = "自媒体图文素材信息", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody MaterialBo bo) {
        return toAjax(iMaterialService.updateByBo(bo));
    }

    /**
     * 删除自媒体图文素材信息
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wemedia:material:remove")
    @Log(title = "自媒体图文素材信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空") @PathVariable String[] ids) {
        return toAjax(iMaterialService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    /**
     * 上传OSS对象存储
     *
     * @param file 文件
     */
    @SaCheckPermission("system:oss:upload")
    @Log(title = "OSS对象存储", businessType = BusinessType.INSERT)
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public R<Void> upload(@RequestPart("file") MultipartFile file) {
        if (ObjectUtil.isNull(file)) {
            throw new ServiceException("上传文件不能为空");
        }
        return toAjax(iMaterialService.upload(file));
    }
}
