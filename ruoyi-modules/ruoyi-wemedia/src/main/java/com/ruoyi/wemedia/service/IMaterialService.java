package com.ruoyi.wemedia.service;

import com.ruoyi.wemedia.domain.Material;
import com.ruoyi.wemedia.domain.vo.MaterialVo;
import com.ruoyi.wemedia.domain.bo.MaterialBo;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;
import java.util.List;

/**
 * 自媒体图文素材信息Service接口
 *
 * @author xiaoqiang
 * @date 2023-02-25
 */
public interface IMaterialService {

    /**
     * 查询自媒体图文素材信息
     */
    MaterialVo queryById(String id);

    /**
     * 查询自媒体图文素材信息列表
     */
    TableDataInfo<MaterialVo> queryPageList(MaterialBo bo, PageQuery pageQuery);

    /**
     * 查询自媒体图文素材信息列表
     */
    List<MaterialVo> queryList(MaterialBo bo);

    /**
     * 修改自媒体图文素材信息
     */
    Boolean insertByBo(MaterialBo bo);

    /**
     * 修改自媒体图文素材信息
     */
    Boolean updateByBo(MaterialBo bo);

    /**
     * 校验并批量删除自媒体图文素材信息信息
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    Boolean upload(MultipartFile file);
}
