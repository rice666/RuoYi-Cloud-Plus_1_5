package com.ruoyi.wemedia.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.util.Date;

/**
 * 文章列表对象 wm_news
 *
 * @author xiaoqiang
 * @date 2022-10-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wm_news")
public class News extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 自媒体用户ID
     */
    private Long userId;
    /**
     * 标题
     */
    private String title;
    /**
     * 图文内容
     */
    private String content;
    /**
     * 文章布局
            0 无图文章
            1 单图文章
            3 多图文章
     */
    private Short type;
    /**
     * 图文频道ID
     */
    private Long channelId;
    /**
     * 标签
     */
    private String labels;
    /**
     * 提交时间
     */
    private Date submitedTime;
    /**
     * 当前状态
            0 草稿
            1 提交（待审核）
            2 审核失败
            3 人工审核
            4 人工审核通过
            8 审核通过（待发布）
            9 已发布
     */
    private Short status;
    /**
     * 定时发布时间，不定时则为空
     */
    private Date publishTime;
    /**
     * 拒绝理由
     */
    private String reason;
    /**
     * 发布库文章ID
     */
    private Long articleId;
    /**
     * 图片
     */
    private String images;
    /**
     * 上下架 0 下架  1 上架
     */
    private Short enable;

    //状态枚举类
    @Alias("WmNewsStatus")
    public enum Status{
        NORMAL((short)0),SUBMIT((short)1),FAIL((short)2),ADMIN_AUTH((short)3),ADMIN_SUCCESS((short)4),SUCCESS((short)8),PUBLISHED((short)9);
        short code;
        Status(short code){
            this.code = code;
        }
        public short getCode(){
            return this.code;
        }
    }



}
