package com.ruoyi.wemedia.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.wemedia.domain.NewsMaterial;
import com.ruoyi.wemedia.domain.bo.NewsMaterialBo;
import com.ruoyi.wemedia.domain.vo.NewsMaterialVo;
import com.ruoyi.wemedia.mapper.NewsMaterialMapper;
import com.ruoyi.wemedia.service.INewsMaterialService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 自媒体图文引用素材信息Service业务层处理
 *
 * @author xiaoqiang
 * @date 2022-10-18
 */
@RequiredArgsConstructor
@Service
public class NewsMaterialServiceImpl implements INewsMaterialService {

    private final NewsMaterialMapper baseMapper;

    /**
     * 查询自媒体图文引用素材信息
     */
    @Override
    public NewsMaterialVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询自媒体图文引用素材信息列表
     */
    @Override
    public TableDataInfo<NewsMaterialVo> queryPageList(NewsMaterialBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<NewsMaterial> lqw = buildQueryWrapper(bo);
        Page<NewsMaterialVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询自媒体图文引用素材信息列表
     */
    @Override
    public List<NewsMaterialVo> queryList(NewsMaterialBo bo) {
        LambdaQueryWrapper<NewsMaterial> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<NewsMaterial> buildQueryWrapper(NewsMaterialBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<NewsMaterial> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getMaterialId() != null, NewsMaterial::getMaterialId, bo.getMaterialId());
        lqw.eq(bo.getNewsId() != null, NewsMaterial::getNewsId, bo.getNewsId());
        lqw.eq(bo.getType() != null, NewsMaterial::getType, bo.getType());
        lqw.eq(bo.getOrd() != null, NewsMaterial::getOrd, bo.getOrd());
        return lqw;
    }

    /**
     * 新增自媒体图文引用素材信息
     */
    @Override
    public Boolean insertByBo(NewsMaterialBo bo) {
        NewsMaterial add = BeanUtil.toBean(bo, NewsMaterial.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改自媒体图文引用素材信息
     */
    @Override
    public Boolean updateByBo(NewsMaterialBo bo) {
        NewsMaterial update = BeanUtil.toBean(bo, NewsMaterial.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(NewsMaterial entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除自媒体图文引用素材信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
