package com.ruoyi.wemedia.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自媒体图文引用素材信息对象 wm_news_material
 *
 * @author xiaoqiang
 * @date 2022-10-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wm_news_material")
public class NewsMaterial extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 素材ID
     */
    private Long materialId;
    /**
     * 图文ID
     */
    private Long newsId;
    /**
     * 引用类型
            0 内容引用
            1 主图引用
     */
    private Integer type;
    /**
     * 引用排序
     */
    private Integer ord;

}
