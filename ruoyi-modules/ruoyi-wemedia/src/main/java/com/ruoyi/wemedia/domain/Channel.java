package com.ruoyi.wemedia.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 频道信息对象 wm_channel
 *
 * @author xiaoqiang
 * @date 2022-10-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wm_channel")
public class Channel extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 频道名称
     */
    private String name;
    /**
     * 频道描述
     */
    private String description;
    /**
     * 是否默认频道
     */
    private Integer isDefault;
    /**
     *
     */
    private Integer status;
    /**
     * 默认排序
     */
    private Integer ord;

}
