package com.ruoyi.wemedia.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.mybatis.core.page.PageQuery;
import com.ruoyi.common.mybatis.core.page.TableDataInfo;
import com.ruoyi.wemedia.domain.Channel;
import com.ruoyi.wemedia.domain.bo.ChannelBo;
import com.ruoyi.wemedia.domain.vo.ChannelVo;
import com.ruoyi.wemedia.mapper.ChannelMapper;
import com.ruoyi.wemedia.service.IChannelService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 频道信息Service业务层处理
 *
 * @author xiaoqiang
 * @date 2022-10-14
 */
@RequiredArgsConstructor
@Service
public class ChannelServiceImpl implements IChannelService {

    private final ChannelMapper baseMapper;

    /**
     * 查询频道信息
     */
    @Override
    public ChannelVo queryById(Integer id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询频道信息列表
     */
    @Override
    public TableDataInfo<ChannelVo> queryPageList(ChannelBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Channel> lqw = buildQueryWrapper(bo);
        Page<ChannelVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询频道信息列表
     */
    @Override
    public List<ChannelVo> queryList(ChannelBo bo) {
        LambdaQueryWrapper<Channel> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Channel> buildQueryWrapper(ChannelBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Channel> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), Channel::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getDescription()), Channel::getDescription, bo.getDescription());
        lqw.eq(bo.getIsDefault() != null, Channel::getIsDefault, bo.getIsDefault());
        lqw.eq(bo.getStatus() != null, Channel::getStatus, bo.getStatus());
        lqw.eq(bo.getOrd() != null, Channel::getOrd, bo.getOrd());
        return lqw;
    }

    /**
     * 新增频道信息
     */
    @Override
    public Boolean insertByBo(ChannelBo bo) {
        Channel add = BeanUtil.toBean(bo, Channel.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改频道信息
     */
    @Override
    public Boolean updateByBo(ChannelBo bo) {
        Channel update = BeanUtil.toBean(bo, Channel.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Channel entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除频道信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Integer> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
