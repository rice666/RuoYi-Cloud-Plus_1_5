package com.ruoyi.wemedia.mapper;

import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.wemedia.domain.News;
import com.ruoyi.wemedia.domain.vo.NewsVo;
import org.springframework.stereotype.Component;

/**
 * 文章列表Mapper接口
 *
 * @author xiaoqiang
 * @date 2022-10-15
 */
@Component
public interface NewsMapper extends BaseMapperPlus<NewsMapper, News, NewsVo> {

}
