package com.ruoyi.user.service;


import com.ruoyi.common.core.domain.R;
import com.ruoyi.user.domain.AppLoginForm;

import java.util.Map;

public interface ApUserService {


    R<Map<String, Object>> login(AppLoginForm appLoginForm);

    void getOldUserList();

}
