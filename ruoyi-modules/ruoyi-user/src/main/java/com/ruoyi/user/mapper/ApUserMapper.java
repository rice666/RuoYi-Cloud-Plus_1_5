package com.ruoyi.user.mapper;


import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.user.domain.ApUser;
import com.ruoyi.user.domain.vo.ApUserVo;

public interface ApUserMapper extends BaseMapperPlus<ApUserMapper, ApUser, ApUserVo> {
}
