package com.ruoyi.user.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.StpUtil;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.system.api.model.LoginUser;
import com.ruoyi.user.domain.AppLoginForm;
import com.ruoyi.user.service.ApUserService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/applogin")
@Slf4j
public class ApUserLoginController {

    @Autowired
    private ApUserService apUserService;


    @PostMapping("/login")
    public R<Map<String, Object>> login(@RequestBody AppLoginForm appLoginForm) {
        log.info("进入了登录方法");
        apUserService.getOldUserList();
        return apUserService.login(appLoginForm);
    }

    /**
     * 登出方法
     */
    @SaCheckPermission("ruoyiuser:appuser:logout")
    @DeleteMapping("/applogout")
    public R<Void> logout() {
        log.info("进入了退出方法！");
        StpUtil.logout();
        return R.ok();
    }
}
