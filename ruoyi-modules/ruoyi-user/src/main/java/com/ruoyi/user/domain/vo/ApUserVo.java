package com.ruoyi.user.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;

import java.io.Serializable;
import java.util.Date;

public class ApUserVo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @ExcelProperty(value = "id")
    private Integer id;

    /**
     * 密码、通信等加密盐
     */
    @ExcelProperty("salt")
    private String salt;

    /**
     * 用户名
     */
    @ExcelProperty("name")
    private String name;

    /**
     * 密码,md5加密
     */
    @ExcelProperty("password")
    private String password;

    /**
     * 手机号
     */
    @ExcelProperty("phone")
    private String phone;

    /**
     * 头像
     */
    @ExcelProperty("image")
    private String image;

    /**
     * 0 男
     1 女
     2 未知
     */
    @ExcelProperty("sex")
    private Boolean sex;

    /**
     * 0 未
     1 是
     */
    @ExcelProperty("is_certification")
    private Boolean certification;

    /**
     * 是否身份认证
     */
    @ExcelProperty("is_identity_authentication")
    private Boolean identityAuthentication;

    /**
     * 0正常
     1锁定
     */
    @ExcelProperty("status")
    private Boolean status;

    /**
     * 0 普通用户
     1 自媒体人
     2 大V
     */
    @ExcelProperty("flag")
    private Short flag;

    /**
     * 注册时间
     */
    @ExcelProperty("created_time")
    private Date createdTime;
}


