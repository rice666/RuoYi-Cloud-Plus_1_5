package com.ruoyi.user.domain;

import lombok.Data;
@Data
public class AppLoginForm {
    /**
     * 手机号  (用户名)
     */
    private String phone;

    /**
     * 密码
     */
    private String password;
}

