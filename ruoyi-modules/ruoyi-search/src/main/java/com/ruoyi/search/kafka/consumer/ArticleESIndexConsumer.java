package com.ruoyi.search.kafka.consumer;

import com.ruoyi.search.domain.SearchArticleVo;

import com.ruoyi.search.esmapper.SearchArticleMapper;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.function.Consumer;

/**
 * 文章管理消息处理
 */
@Slf4j
@Component
public class ArticleESIndexConsumer {

    @Autowired
    private SearchArticleMapper searchArticleMapper;
    /**
     * 文章上下架
     */
    @Bean
    Consumer<SearchArticleVo> articleindex() {

        log.info("初始化订阅articleindex");
        return vo -> {
            if(vo != null){
                searchArticleMapper.insert(vo);
                log.info("文章成功索引："+vo.getTitle());
            }
        };
    }



}
