package com.ruoyi.search.esmapper;

import cn.easyes.core.conditions.interfaces.BaseEsMapper;
import com.ruoyi.search.domain.SearchArticleVo;

public interface SearchArticleMapper extends BaseEsMapper<SearchArticleVo> {
}
