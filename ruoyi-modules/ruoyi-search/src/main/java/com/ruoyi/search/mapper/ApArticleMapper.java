package com.ruoyi.search.mapper;


import com.ruoyi.article.api.domain.ApArticle;
import com.ruoyi.article.api.domain.vo.ApArticleVo;
import com.ruoyi.common.mybatis.core.mapper.BaseMapperPlus;
import com.ruoyi.search.domain.SearchArticleVo;


import java.util.List;

public interface ApArticleMapper extends BaseMapperPlus<ApArticleMapper, ApArticle, SearchArticleVo> {
    public List<SearchArticleVo> loadArticleList();

}
